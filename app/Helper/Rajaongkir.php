<?php
/**
 * Created by ahmad.
 * Date: 4/4/19
 * Time: 10:10 PM
 */

namespace App\Helper;


class Rajaongkir
{
    const API = "0df6d5bf733214af6c6644eb8717c92c";

    private $curl;

    public function init() {
        $this->curl = curl_init();
        curl_setopt_array($this->curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: ". self::API
            ),
        ));
    }

    public function retrieveProvince() {
        curl_setopt($this->curl, CURLOPT_URL, "https://api.rajaongkir.com/starter/province");
    }

    public function findProvince($id) {
        curl_setopt($this->curl, CURLOPT_URL, "https://api.rajaongkir.com/starter/province?id=". $id);
    }

    public function retrieveCity() {
        curl_setopt($this->curl, CURLOPT_URL, "https://api.rajaongkir.com/starter/city");
    }

    public function findCity($id) {
        curl_setopt($this->curl, CURLOPT_URL, "https://api.rajaongkir.com/starter/city?id=". $id);
    }

    public function exec() {
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);

        curl_close($this->curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

}