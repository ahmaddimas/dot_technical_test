# Langkah instalasi
1. membuat database dengan nama dot
2. menjalankan artisan command migrate
3. menjalankan artisan command fetch:data untuk fetch data dari raja ongkir


# Jawaban Knowledge & Experience Questions
1. mengerjakan suatu masalah yang belum pernah saya temui, dan mengerjakan suati aplikasi dengan teknologi yang belum pernah saya pelajari, dan untuk mengatasinya saya akan mencari solusi dari berbagai sumber di internet  
2. membuang variable dan kode yang tidak diperlukan dan membuat method yang dipakai berulang kali
3. commit setiap hasil pekerjaan yang sudah diselesaikan dengan syarat tidak ada error
4. suatu struktur penulisan kode dalam pemrograman
5. bersedia, jika memang harus remote saya akan mengkonfirmasi terlebih dahulu waktu untuk kolaborasi pengerjaan project