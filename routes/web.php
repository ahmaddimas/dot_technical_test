<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\City;
use App\Helper\Rajaongkir;
use App\Province;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    // Database
    Route::get('/search/provinces', function (\Illuminate\Http\Request $request) {
        $provinceId = \Illuminate\Support\Facades\Input::get('province_id');
        if ($provinceId) {
            $result = Province::find($provinceId);
            echo json_encode(['result' => $result]);
        }
    });
    Route::get('/search/cities', function (\Illuminate\Http\Request $request) {
        $cityId = \Illuminate\Support\Facades\Input::get('city_id');
        if ($cityId) {
            $result = City::find($cityId);
            echo json_encode(['result' => $result]);
        }
    });
    // API Rajaongkir
    Route::get('/rajaongkir/search/provinces', function (\Illuminate\Http\Request $request) {
        $provinceId = \Illuminate\Support\Facades\Input::get('province_id');
        if ($provinceId) {
            $rajaOngkir = new Rajaongkir();
            $rajaOngkir->init();
            $rajaOngkir->findProvince($provinceId);
            echo $rajaOngkir->exec();
        }
    });
    Route::get('/rajaongkir/search/cities', function (\Illuminate\Http\Request $request) {
        $cityId = \Illuminate\Support\Facades\Input::get('city_id');
        if ($cityId) {
            $rajaOngkir = new Rajaongkir();
            $rajaOngkir->init();
            $rajaOngkir->findCity($cityId);
            echo $rajaOngkir->exec();
        }
    });
});