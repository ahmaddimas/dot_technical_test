<?php
/**
 * Created by ahmad.
 * Date: 4/4/19
 * Time: 8:31 PM
 */

$S = [2, 1, 6, 9, 9, 4, 3];

// sort descdending
rsort($S);
$temp = 0;
$count = 0;
for ($i = 0; $i < count($S); $i++) {
    if ($temp == $S[$i])
        continue;
    $temp = $S[$i];
    $count++;
    if ($count == 2)
        break;
}
echo $temp;
